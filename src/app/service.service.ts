import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  user: firebase.User;

  private upurl = "http://localhost:3000/register";
  private loginurl = "http://localhost:3000/login";

  constructor(
    private router:Router,
    private afAuth:AngularFireAuth,
    private http:HttpClient,

  ) { }

  registerUser(user) {
    return this.http.post<any>(this.upurl, user)
  }

    ////////_____ Login User ____////////
    LoginUsers(user) {
      console.log('LoginUsers in service')
      return this.http.post<any>(this.loginurl, user)
    }

//////// Logig Firebase      ///////////////////
    //////_____ Login Google ____///////
  loginGoogle() {
    console.log('Redirecting to Google login provider')
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
 
////////////_____ Login Facebook _____/////////
  loginFacebook() {
    console.log('Redirecting to Facebook login provider')
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());

  }

  getLoggedInUser() {
    return this.afAuth.authState;
  }
  
}
