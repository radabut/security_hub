import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  user: firebase.User;
  image:any
  constructor(
    private afAuth:AngularFireAuth,
    private service:ServiceService,
    private router:Router,
  ) { }

  ngOnInit(): void {
    this.service.getLoggedInUser().subscribe(user => {
      console.log(user)

      if(user != null){
      this.user == user;
      this.image = user.photoURL
      console.log(this.image)
      }else{
        this.router.navigateByUrl('/Security_Hub/login')
      }

    });

  }

  logout() {
    this.afAuth.auth.signOut();
    // this.router.navigateByUrl('/Security_Hub/login')
  }


}
