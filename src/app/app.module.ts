import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';





import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { environment } from '../environments/environment'
import { HttpClientModule} from '@angular/common/http'
// import { GoogleLoginProvider, FacebookLoginProvider, SocialLoginModule, AuthServiceConfig, } from 'angularx-social-login'
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRouting } from './app.routing';
import { RegisterComponent } from './register/register.component';
import { HomepageComponent } from './homepage/homepage.component';


// let config = new AuthServiceConfig([
//   {
//     id: GoogleLoginProvider.PROVIDER_ID,
//     provider: new GoogleLoginProvider("503841791810-ifbrajormmnctqcbc0060fto93ertoig.apps.googleusercontent.com")
//   },
//   {
//     id: FacebookLoginProvider.PROVIDER_ID,
//     provider: new FacebookLoginProvider("222630895834643")
//   }
// ]);

// export function provideConfig() {
//   return config;
// }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomepageComponent
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    AppRouting,
    FormsModule,
    HttpClientModule,
    // SocialLoginModule,
  ],
  providers: [
  //   {
  //     provide: AuthServiceConfig,
  //     useFactory: provideConfig
  // }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
