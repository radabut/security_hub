import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomepageComponent } from './homepage/homepage.component';




const routes: Routes = [
  { path: '', redirectTo: 'Security_Hub/login', pathMatch: 'full' },
  { path: 'Security_Hub/login', component: LoginComponent },
  { path: 'Security_Hub/register', component: RegisterComponent },
  { path: 'Security_Hub/homepage', component: HomepageComponent },


];


export const AppRouting = RouterModule.forRoot(routes);