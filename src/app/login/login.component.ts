import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';
// import { SocialUser, AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:any
  password:any

  user: firebase.User;

  // user: SocialUser;

  constructor(
    // private authService: AuthService,
    private service:ServiceService,
    private router:Router,
  ) { }

  ngOnInit(): void {
    this.service.getLoggedInUser().subscribe(user => {
      this.user = user;
      if(user != null){
        console.log(user);
        this.router.navigateByUrl('/Security_Hub/homepage')
      }
    });

  }

  LoginUser() {
    // const loginUserr = {
    //   "email": this.email,
    //   "password": this.password,
    // }
    // this.service.LoginUsers(loginUserr)
    //   .subscribe(
    //     res => {
    //       console.log("LOGIN...." + res)
    //       localStorage.setItem("email", res.email);
    //       localStorage.setItem("Id", res._id)
    //       this.router.navigateByUrl('/Security_Hub/homepage')

    //         .then(() => {
    //           location.reload();
    //         });
    //     },
    //     err => {
    //       console.log(err)
    //       alert('ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง')
    //     }
    //   )
  }

  ///////////////______Login Google___________/////////
  LoginGoogle() {
    console.log('Login...');
    this.service.loginGoogle();
}
//////////////______Login Facebook _____//////////////
LoginFacebook() {
  console.log('Login...');
  this.service.loginFacebook();
}

}
