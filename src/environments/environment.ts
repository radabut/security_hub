// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBjZEpuZxtbzy8nVU42mWQWiI6Xp0WVbCg",
    authDomain: "test-security-hub-d9ac8.firebaseapp.com",
    databaseURL: "https://test-security-hub-d9ac8.firebaseio.com",
    projectId: "test-security-hub-d9ac8",
    storageBucket: "test-security-hub-d9ac8.appspot.com",
    messagingSenderId: "824215731851",
    appId: "1:824215731851:web:b09927f6e649fdf5f5de8e",
    measurementId: "G-B7W4RY4MY4"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
